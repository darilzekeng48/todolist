<?php 
require("_header.php");

if (isset($_GET['q']) AND isset($_GET['id'])) {
	$q = $_GET['q'];
	$id = intval($_GET['id']);
	if ($q == 'modifier') {
		$key = 'Faite';
		$app->modifTache($id, $key);
		$app->redirect('index.php');
	}else if ($q == 'supprimer') {
		$app->deleteColumn('taches', 'id', $id);	
		$app->redirect('index.php');
	}
	
}else{
	$app->redirect('index.php');
}


?>