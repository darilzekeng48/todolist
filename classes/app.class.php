<?php
/**
* class App
* Permet de gérer les configurations diverses
*/

class App{

	/**
	* @var string instance de connexion à la BDD
	*/
	private $DB;

	public function __construct($DB){
		$this->DB = $DB;
	}
	
	/**
	*faire une redirection et un exit
	*@param String $url adresse ou on doit rediriger l'utilisateur
	*/
	public static function redirect($url){
	    if(is_file($url) OR is_dir($url)){
	        header('Location: '.$url);
	        exit();        
	    }
	}

	function modifTache($id, $key){
	    $exist = $this->DB->query("SELECT * FROM taches WHERE id=?", array($id));

	    if ($exist) {
	    	$this->DB->insert("UPDATE  taches SET statut=? WHERE id=?", array($key, $id));
	    }else{

	    }
	}

	public function getTache(){
		$req = "SELECT * FROM taches";
		$tache = $this->DB->query($req);

		return $tache;
	}

	/**
	*Supprimer les enregistrement d'une table
	*@param $table nom de la table dans la BD
	*@param $column nom de la colone a utiliser
	*@param $value valeur du champ a supprimer
	*@return $status int déterminer si tout s'est bien passé ou pas
	*/
	function deleteColumn($table, $column, $value){
		$status = -1;
		$exist = $this->DB->query("SELECT {$column} FROM {$table} WHERE {$column}={$value}");
		if ($exist) {
			$this->DB->insert("DELETE FROM {$table} WHERE {$column}={$value}");
			$status = 0;
		}
		return $status;
	}

	function isConnect(){
		if (isset($_SESSION['auth'])) {
			return true;
		}else{
			return false;
		}
	}

}
