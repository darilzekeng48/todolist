<?php 
require("../_header.php");
?>

<?php

if (isset($_POST) && !empty($_POST) && !empty($_POST['mail']) && !empty($_POST['password'])) {
	$errors = array();
	$password = sha1($_POST['password']);
	$user = $DB->query('SELECT * FROM membres WHERE mail=:mail AND password=:password', array('mail'=>$_POST['mail'], 'password'=>$password));

	if ($user) {	
		foreach ($user as $user1) {
			$utilisateur = $user1;
		}

		$_SESSION['auth'] = $utilisateur;
		$_SESSION['flash']['success'] = 'Vous êtes maintenant connecté au site '; echo "bienvenu";

		header('Location: ../index.php');
		debug($utilisateur);
		
	}else{
		$errors['identifiant'] = "Vos identifiants sont invalides";
		$_SESSION['flash']['danger'] = 'Vos identifiants sont invalides';
	}
}else{
	$errors['champ'] = 'Veuillez renseigner votre mail et votre mot de passe';
	$_SESSION['flash']['danger'] = 'Veuillez renseigner votre mail et votre mot de passe';
}


?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Connexion</title>
    <link rel="shortcut icon" href="css/favicon.png">

    <link href="../css/style.css" rel="stylesheet">
  </head>

  <body  style="background-color:white;">
		<div id="contenu">
			<!-- Affichage des erreurs -->
			<?php if (!empty($errors)): ?>
			<div class="alert-danger">
				<p>Vous n'avez pas rempli correctement le formulaire</p>
				<ul>
				<?php foreach($errors as $error): ?>
					<li><?= $error; ?></li>
				<?php endforeach; ?>
			</ul>
			</div>
			<?php endif; ?>


			<div id="utilisateur" class="cadre" style="position: relative; width: 60%; margin: auto;">

				<form action="" method="POST" role="form">
					<div >
						<label class="" for="mail">Adresse email</label>
					   <input type="email" class="form-input" name="mail" id="mail">
					</div>
					<div class="">
						<label for="password">Mot de passe</label>
					   <input type="password" class="form-input" name="password" id="password">
					</div>
				   <div class="">
					<a href="inscription.php">Créer un compte ?</a>
				  </div><br>
					<div class="">
						<button type="submit" name="utilisateur" class="btn">Connexion</button>
						<button type="reset" class="btn">Annuler</button>
					</div>
				</form>
			</div> <!-- /utilisateur-->

		</div><!-- /Contenu -->

  </body>
</html>
