<?php 
require("../_header.php");
?>

<?php
if (!empty($_POST)) {
	$errors = array();

	$nom = "";
	$prenom = "";
	$sexe = "Masculin";
	$date_naiss = "";
	$numero = "";
	$mail = "";
	$mdp = "";
	$cmdp = "";


	if (isset($_POST['utilisateur'])) {
		$nom = $_POST['nom'];
		$prenom = $_POST['prenom'];
		//$sexe = $_POST['sexe_utilisateur'];
		$date_naiss = $_POST['annee_utilisateur'].'-'.$_POST['mois_utilisateur'].'-'.$_POST['jour_utilisateur'];
		$numero = $_POST['numero'];
		$mail = $_POST['mail'];
		$mdp = $_POST['password'];
		$cmdp = $_POST['cmdp'];
		
	}

	if (isset($_POST['utilisateur'])) {
		if (empty($nom) || !preg_match("/^[a-zA-Z0-9 \-_]+$/", $nom)) {
			$errors['nom'] = "Votre nom n'est pas valide";
		}

		if (empty($prenom) || !preg_match("/^[a-zA-Z0-9 \-_]+$/", $prenom)) {
			$errors['prenom'] = "Votre prenom n'est pas valide";
		}

		if (!isset($_POST['sexe_utilisateur'])) {
			$errors['sexe'] = "Veullez renseigner votre sexe";
		}

		if (empty($date_naiss)) {
			$errors['date_naiss'] = "veuillez entrer votre date de naissance";
		}

		if (empty($numero) || !preg_match("/^[0-9]+$/", $numero)) {
			$errors['numero'] = "Votre numéro n'est pas valide";
		}

		if (empty($mail) || !filter_var($mail, FILTER_VALIDATE_EMAIL)) {
			$errors['mail'] = "Votre email n'est pas valide";
		}else{
			$req = $DB->query('SELECT id FROM membres WHERE mail=:mail', array('mail'=>$mail));

			if ($req) {
				$errors['mail'] = "Cette adresse email est déja utilisée";	
			}
		}

		if (empty($mdp) || $mdp != $cmdp) {
			$errors['mdp'] = "Vous devez renseigner un mot de passe valide";
		}

		if ($_POST['annee_utilisateur']=="jj" || $_POST['mois_utilisateur']=="mm" || $_POST['jour_utilisateur']=="aaaa") {
			$errors['date_naiss'] = 'veuillez renseigner votre date de naissance';
		}

		if (empty($errors)){

		    // On enregistre les informations dans la base de données 
		    $mdp = sha1($mdp);
		    /*$req = $DB->insert('INSERT INTO membres SET nom=:nom, prenom=:prenom, sexe=:sexe, date_naiss=:date_naiss, numero=:numero, mail=:mail, password=:password', 
		    	array('nom'=>$nom, 'prenom'=>$prenom, 'sexe'=>$sexe, 'date_naiss'=>$date_naiss, 'numero'=>$numero, 'mail'=>$mail, 'password'=>$mdp)
		    	);*/

		    // On redirige l'utilisateur vers la page de login avec un message flash
		    $_SESSION['flash']['success'] = 'Votre compte a bien été créer';
		    header('Location: connexion.php');
		    exit(); 

		}
	}
}
?>

<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Inscription</title>

    <link rel="shortcut icon" href="css/favicon.png">

    <link href="../css/style.css" rel="stylesheet">

  </head>

  <body  style="background-color:white;">


		<div id="contenu" class="">
			<!-- Affichage des erreurs -->
			<?php if (!empty($errors)): ?>
			<div class="alert-danger">
				<p>Vous n'avez pas rempli correctement le formulaire</p>
				<ul>
				<?php foreach($errors as $error): ?>
					<li><?= $error; ?></li>
				<?php endforeach; ?>
			</ul>
			</div>
			<?php endif; ?>


			<div id="utilisateur" style="position: relative; width: 60%; margin: auto;">

				<form action="" method="POST" role="form">
					<div >
						<label class="" for="nom">Nom</label>
					   <input type="text" class="form-input" name="nom" id="nom" size="10" value="<?php if(isset($_POST['nom'])){echo $_POST['nom'];} ?>">
					</div>
					<div>
						<label class="" for="prenom">Prénom</label>
					   <input type="text" class="form-input" name="prenom" id="prenom" size="10" value="<?php if(isset($_POST['prenom'])){echo $_POST['prenom'];} ?>">
					</div>
					<div class="">
						<label for="prenom">Sexe</label><br>
					   <label><input type="radio" name="sexe_utilisateur" value="masculin">Masculin</label>
					   <label><input type="radio" name="sexe_utilisateur" value="feminin">Feminin</label>
					</div>
				   <div class="">
						<label class="" for="date_naiss_utilisateur">Date naiss utilisateur</label><br>
						<div class="inline">
						   <select name="jour_utilisateur" required>
								<option value="jj">JJ</option>
								<?php
								$nb_jours = 31;
								$jour = 01;
								while($jour <= $nb_jours){
									echo '<option value="'.$jour.'">' .$jour.'</option>';
									$jour++;
								}
								?>
						   </select>
						</div>
						<div class="inline">
						   <select name="mois_utilisateur">
								<option value="mm">MM</option>
								<option value="01">Janvier</option>
								<option value="02">Février</option>
								<option value="03">Mars</option>
								<option value="04">Avril</option>
								<option value="05">Mai</option>
								<option value="06">Juin</option>
								<option value="07">Juillet</option>
								<option value="08">Aout</option>
								<option value="09">Septembre</option>
								<option value="10">Octobre</option>
								<option value="11">Novembre</option>
								<option value="12">Decembre</option>

						   </select>
						</div>
						<div class="inline">
						   <select name="annee_utilisateur">
								<option value="aaaa">AAAA</option>
								<?php
								$nb=100;
								$annee = date('Y');
								$annee_deb = $annee-100;
								$annee_cours = $annee_deb;
								while($annee_cours <= $annee){
									echo '<option value="'.$annee_cours.'">' .$annee_cours.'</option>';
									$annee_cours++;
								}
								
								?>
						   </select>
						</div>
					</div>
					<div class="">
						<label for="numero_de_telephone">Numéro de téléphone</label>
					   <input type="tel" class="form-input" name="numero" id="numero" size="10" value="<?php if(isset($_POST['numero'])){echo $_POST['numero'];} ?>">
					</div>
					<div class="">
						<label for="mail">Adresse email</label>
					   <input type="mail" class="form-input" name="mail" id="mail" size="10" value="<?php if(isset($_POST['mail'])){echo $_POST['mail'];} ?>">
					</div>
					<div class="">
						<label for="mot_de_passe">Mot de passe</label>
					   <input type="password" class="form-input" name="password" id="password" size="10">
					</div>
					<div class="">
						<label for="cmdp">Confirmer mot de passe</label>
					   <input type="password" class="form-input" name="cmdp" id="cmdp" size="10">
					</div>
					<div class="">
						<button type="submit" name="utilisateur" class="btn">Confirmer</button>
						<button type="reset" class="btn">Annuler</button>
					</div>
				</form>
			</div> <!-- /utilisateur-->

		</div><!-- /Contenu -->
	
  </body>
</html>
