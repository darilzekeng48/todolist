<?php 
require("_header.php");
?>

<?php

if (!$app->isConnect()) {
	$app->redirect("connexion/connexion.php");	
}
$auth = $_SESSION['auth'];

if (!empty($_POST)) {
	$errors = array();

	$nom = "";
	$description = "";
	$statut = "En cours";
	$date_debut = "";
	$date_fin = "";
	$id_utilisateur = 1;

	if (isset($_POST['creer_tache'])) {
		$nom = $_POST['nom'];
		$description = $_POST['description'];
		
	}

	if (isset($_POST['creer_tache'])) {
		if (empty($nom) || !preg_match("/^[a-zA-Z0-9 \-_]+$/", $nom)) {
			$errors['nom'] = "Ce nom n'est pas valide";
		}

		if (empty($errors)){

		    // On enregistre les informations dans la base de données 
		    $req = $DB->insert('INSERT INTO taches SET nom=:nom, description=:description, statut=:statut, date_debut=NOW(), date_fin=(NOW() +INTERVAL 1 DAY), id_utilisateur=:id_utilisateur', 
		    	array('nom'=>$nom, 'description'=>$description, 'statut'=>$statut, 'id_utilisateur'=>$id_utilisateur)
		    	);
		    header('Location: index.php');

		}
	}
}
?>

<?php 
$taches = $DB->query("SELECT * FROM taches WHERE statut=?", array('En cours'));

//
foreach ($taches as $t) {
	//$date = date('Y-m-d H:i:s');
	$date = date('Y-m-d H:i:s', time());
	$date_fin = date('Y-m-d H:i:s', strtotime($t->date_fin));
	if ($date >= $date_fin) {
		$id = intval($t->id);
		$app->modifTache($id, 'Bloquée');
	}
}

$tacheC = $DB->query("SELECT * FROM taches WHERE statut=?", array('En cours'));
$tacheB = $DB->query("SELECT * FROM taches WHERE statut=?", array('Bloquée'));
$tacheF = $DB->query("SELECT * FROM taches WHERE statut=?", array('Faite'));
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mes taches</title>

    <link rel="shortcut icon" href="css/favicon.png">

    <link href="css/style.css" rel="stylesheet">
</head>
<body>

<div id="contenu">
	<div id="">
		<h2>Todolist PHP, HTML, Javascript et CSS </h2>
		<h3>Vous êtes connecté en tant que <span><?= $auth->nom.' '.$auth->prenom ?></span></h3>
	</div>

	<div id="utilisateur" class="cadre" style="position: relative; width: 60%; margin: auto;">
		<form action="" method="POST" role="form">
			<div >
				<label class="" for="nom">Nom de la tache</label>
			   <input type="text" class="form-input" name="nom" id="nom" required>
			</div>
			<div class="">
				<label for="description">Description</label>
			   <input type="text" class="form-input" name="description" id="description">
			</div>
			<div class="">
				<button type="submit" name="creer_tache" class="btn">Créer la tâche</button>
			</div>
		</form>
	</div> <!-- /utilisateur-->

	<div class="cadre">
		<h3 class="head  text-blue">Liste des tâches en cours</h3>
		<table class="table">
			<thead>
				<tr>
					<th>#</th>
					<th>Nom tache</th>
					<th>Description</th>
					<th>Statut</th>
					<th>Délai</th>
					<th>Action</th>		
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; ?>
				<?php foreach($tacheC as $tache): ?>
				<tr>
					<td>1</td>
					<td><?= $tache->nom ?></td>
					<td><?= $tache->description ?></td>
					<td><?= $tache->statut ?></td>
					<td><?= $tache->date_fin ?></td>
					<td>
						<a class="btn btn-faire" href="action?id=<?= $tache->id ?>&q=modifier" title="Faite">Faire</a>&ensp;
						<a class="btn btn-sup" href="action?id=<?= $tache->id ?>&q=supprimer" title="Supprimer">Supprimer</a>
					</td>
				</tr>
				<?php $i = $i+1; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>

	<div id="taches-autres">
		<div class="cadre ot1">
			<h3 class="head">Listes des taches éffectuées</h3>
			<br>
			<table class="table" style="color: black;">
				<thead>
					<tr>
						<th>#</th>
						<th>Nom tache</th>
						<th>Description</th>
						<th>Action</th>		
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; ?>
					<?php foreach($tacheF as $tache): ?>
					<tr>
						<td><?= $i ?></td>
						<td><?= $tache->nom ?></td>
						<td><?= $tache->description ?></td>
						<td>
							<a class="btn btn-sup" href="action?id=<?= $tache->id ?>&q=supprimer" title="Supprimer">Supprimer</a>
						</td>
					</tr>
					<?php $i = $i+1; ?>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
		<div class="cadre ot2">
			<h3 class="head  text-red">Liste des taches bloquées</h3>
			<br>
			<table class="table" style="color: black;">
				<thead>
					<tr>
						<th>#</th>
						<th>Nom tache</th>
						<th>Description</th>
						<th>Action</th>		
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; ?>
					<?php foreach($tacheB as $tache): ?>
					<tr>
						<td><?= $i ?></td>
						<td><?= $tache->nom ?></td>
						<td><?= $tache->description ?></td>
						<td>
							<a class="btn btn-sup" href="action?id=<?= $tache->id ?>&q=supprimer" title="Supprimer">Supprimer</a>
						</td>
					</tr>
					<?php $i = $i+1; ?>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
	var n = 3600;//temps d'attente soit 1 heurs
 	var timeout = setTimeout("location.reload(true);",n*1000);
 	function resetTimeout() {
		clearTimeout(timeout);
		timeout = setTimeout("location.reload(true);",n*1000);
 	}
</script>
</body>
</html>