-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mer 26 Mai 2021 à 07:43
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `todolist`
--

-- Structure de la table `membres`
--

CREATE TABLE IF NOT EXISTS `membres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(200) NOT NULL,
  `prenom` varchar(200) NOT NULL,
  `sexe` varchar(10) NOT NULL,
  `date_naiss` varchar(200) NOT NULL,
  `numero` int(11) NOT NULL,
  `mail` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `membres`
--

INSERT INTO `membres` (`id`, `nom`, `prenom`, `sexe`, `date_naiss`, `numero`, `mail`, `password`) VALUES
(1, 'zenkus', 'Darryl', 'Masculin', '1921-01-1', 2147483647, 'zenkus@yahoo.fr', '4b576c733d055a8610b615c4935a4eb33b3ca045');

-- --------------------------------------------------------

--
-- Structure de la table `taches`
--

CREATE TABLE IF NOT EXISTS `taches` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_utilisateur` int(11) NOT NULL,
  `nom` varchar(200) NOT NULL,
  `description` varchar(255) NOT NULL,
  `statut` varchar(200) NOT NULL,
  `date_debut` datetime NOT NULL,
  `date_fin` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `taches`
--

INSERT INTO `taches` (`id`, `id_utilisateur`, `nom`, `description`, `statut`, `date_debut`, `date_fin`) VALUES
(5, 1, 'dormir', 'humm', 'Faite', '2021-05-26 05:32:49', '2021-05-27 05:32:49'),
(9, 1, 'zenkus', '', 'Bloquée', '2021-05-26 07:24:10', '2021-05-27 07:24:10'),
(10, 1, 'zenkus2', '', 'En cours', '2021-05-26 08:08:22', '2021-05-27 08:08:22'),
(11, 1, 'A bloquer', '', 'Bloquée', '2021-05-26 08:13:35', '2021-05-26 06:55:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
